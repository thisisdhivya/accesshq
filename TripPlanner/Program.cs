using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Threading;

namespace TripPlanner
{
    class Program
    {
        public const string fromstation = "Central station, sydney";
        public const string tostation = "Wynyard station";
        static void Main(string[] args)
        {
        }
        
        IWebDriver driver = new ChromeDriver();

        [SetUp]
        public void Initialize()
        {
            //Navigate to Execute automation demo page
            driver.Navigate().GoToUrl("https://transportnsw.info/trip");
            driver.Manage().Window.Maximize();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            Console.WriteLine("Opened URL");
        }


        [Test]
        public void TestTripPlanner ()
        {
            for (var i = 0; i < 100; i++)
            {
                IWebElement fromfield = driver.FindElement(By.Name("search-input-From"));
                IWebElement tofield = driver.FindElement(By.Name("search-input-To"));
                IWebElement button = driver.FindElement(By.Id("search-button"));
                if (fromfield != null && tofield != null)
                {
                    break;
                };
                Thread.Sleep(1000);
            }
            FieldInputSetter.InputText(driver, "search-input-From", fromstation, FieldType.Name);
            Thread.Sleep(1000);
            FieldInputSetter.InputText(driver, "search-input-To", tostation, FieldType.Name);
            Thread.Sleep(1000);
            FieldInputSetter.Click(driver, "search-button", FieldType.ID);
            //driver.FindElement(By.XPath("//button[contains(text(), 'Go')]")).Click();
            Thread.Sleep(1000);
            IWebElement successfield = driver.FindElement(By.Id("tp-result-list"));
            Assert.IsNotNull(successfield);
            Console.WriteLine("Executed Test");
        }


        [TearDown]
        public void CleanUp()
        {
            Thread.Sleep(3000);
            driver.Close();
            Console.WriteLine("Closed the browser");
        }
    }
}
