﻿//Class to feed input for the fields.
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace TripPlanner
{
    public enum FieldType
    {
        ID,
        Name,
        Css
    }
    class FieldInputSetter
    {
        //Method to input text to form fields
        public static void InputText(IWebDriver driver, string field, string value, FieldType fieldtype)
        {
            switch (fieldtype)
            {
                case (FieldType.ID):
                    {
                        driver.FindElement(By.Id(field)).SendKeys(value);
                        break;
                    }
                case (FieldType.Name):
                    {
                        driver.FindElement(By.Name(field)).SendKeys(value);
                        break;
                    }
                default:
                    break;
            }
        }

        //Method to perform click action.
        public static void Click(IWebDriver driver, string field, FieldType fieldtype)
        {
            switch (fieldtype)
            {
                case (FieldType.ID):
                    {
                        driver.FindElement(By.Id(field)).Click();
                        break;
                    }
                case (FieldType.Name):
                    {
                        driver.FindElement(By.Name(field)).Click();
                        break;
                    }
                default:
                    break;
            }            
        }

        //Selecting a item from drop down
        public static void SelectDropDown(IWebDriver driver, string field, string value, FieldType fieldtype)
        {
            switch (fieldtype)
            {
                case (FieldType.ID):
                    {
                        new SelectElement(driver.FindElement(By.Id(field))).SelectByText(value);
                        break;
                    }
                case (FieldType.Name):
                    {
                        new SelectElement(driver.FindElement(By.Name(field))).SelectByText(value);
                        break;
                    }
                case (FieldType.Css):
                    {
                        new SelectElement(driver.FindElement(By.ClassName(field))).SelectByText(value);
                        break;
                    }
                default:
                    break;
            }   
        }
    }
}
