﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;
using Newtonsoft.Json.Linq;

namespace StopFinder
{
    class Program
    {
        //Not Valid code using this test fails.
        public const string cstopcodeinvalid = "23";
        //Valid code using this test succeeds.
        public const string cstopcodecentral = "10101100";
        public const string ctopcodewynyard = "10101102";

        static void Main(string[] args)
        {
        }

        [Test]
        public async Task TestStopFinder()
        {
            using (var client = new HttpClient())
            {
                string url = "https://www.transportnsw.info/web/XML_STOPFINDER_REQUEST?TfNSWSF=true&language=en&name_sf={0}&outputFormat=rapidJSON&type_sf=any&version=10.2.2.48";
                // Change the Constant String to test the fail scenario
                url = string.Format(url, ctopcodewynyard);

                var request = new HttpRequestMessage
                {
                    RequestUri = new Uri(url),
                    Method = HttpMethod.Get
                };

                request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                using (var response = await client.SendAsync(request))
                {
                    string responseBody = await response.Content.ReadAsStringAsync();
                    JObject joResponse = JObject.Parse(responseBody);
                    if (joResponse != null)
                    {
                        JArray ojSysMsg = (JArray)joResponse["systemMessages"];
                        if (ojSysMsg != null && ojSysMsg.Count > 0)
                        {
                            string code = ojSysMsg[0]["code"].ToString();
                            // This is the error code returned by Transport NSW for invalid stop IDs
                            Assert.AreNotEqual(code, "-8011"); 
                        }
                    }
                }
            }
        }
    }
}